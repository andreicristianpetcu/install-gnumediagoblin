# base image 0.9.9
echo "export GMG_DOMAIN=filme.ceata.org
export GMG_SENDER_EMAIL=gnumediagoblin@ceata.org" | sudo tee /etc/profile.d/gnumediagoblin.sh

sudo chmod +x /etc/profile.d/gnumediagoblin.sh
sudo chmod o+r /etc/profile.d/gnumediagoblin.sh

source /etc/profile.d/gnumediagoblin.sh

sudo apt-get install git-core python python-dev python-lxml python-imaging python-virtualenv postgresql postgresql-client python-psycopg2 nginx-full wget -y

#packages for plugins
#video
sudo apt-get install python-gst0.10 gstreamer0.10-plugins-base gstreamer0.10-plugins-bad gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly gstreamer0.10-ffmpeg -y
#audio
sudo apt-get install python-gst0.10 gstreamer0.10-plugins-{base,bad,good,ugly} gstreamer0.10-ffmpeg python-numpy python-scipy libsndfile1-dev libasound2-dev -y
#pdf and office stuff
sudo apt-get install poppler-utils unoconv libreoffice-common -y

sudo -u postgres createuser -A -D --no-createrole mediagoblin

sudo -u postgres createdb -E UNICODE -O mediagoblin mediagoblin

sudo adduser --system mediagoblin

sudo groupadd mediagoblin

sudo usermod -a -G mediagoblin mediagoblin
sudo usermod -a -G mediagoblin $USER

sudo mkdir -p /srv/$GMG_DOMAIN && sudo chown -hR mediagoblin:mediagoblin /srv/$GMG_DOMAIN

sudo -u mediagoblin /bin/bash

source /etc/profile.d/gnumediagoblin.sh

cd /srv/$GMG_DOMAIN

git clone git://gitorious.org/mediagoblin/mediagoblin.git
cd mediagoblin
git submodule init && git submodule update

(virtualenv --system-site-packages . || virtualenv .) && ./bin/python setup.py develop

./bin/easy_install flup

cp mediagoblin.ini mediagoblin_local.ini

sed -i "s/email_sender_address.*/email_sender_address = \"$GMG_SENDER_EMAIL\"/g" mediagoblin_local.ini
sed -i "s/.*sql_engine.*/sql_engine = postgresql:\/\/\/mediagoblin/g" mediagoblin_local.ini

./bin/pip install scikits.audiolab
./bin/gmg dbupdate

# ./lazyserver.sh --server-name=broadcast

cd /srv/$GMG_DOMAIN/
wget https://gitlab.com/andreicristianpetcu/install-gnumediagoblin/raw/master/nginx.conf
sed -i "s/mediagoblin.example.org/$GMG_DOMAIN/g" nginx.conf
exit 

source /etc/profile.d/gnumediagoblin.sh

# sudo ln -s /srv/$GMG_DOMAIN/nginx.conf /etc/nginx/conf.d/
sudo ln -s /srv/$GMG_DOMAIN/nginx.conf /etc/nginx/sites-enabled/"$GMG_DOMAIN".nginx.conf

# sudo chmod g+r /srv/$GMG_DOMAIN/mediagoblin/user_dev/crypto/itsdangeroussecret.bin 

sudo /etc/init.d/nginx restart

# <mgv> si anume chmod 755 /srv/ceata.org/medii/mediagoblin/user_dev
# <mgv> toate fisierele din user_dev sunt 644
# <mgv> dar user_dev in sine e 700

sudo -u mediagoblin /bin/bash
source /etc/profile.d/gnumediagoblin.sh
cd /srv/$GMG_DOMAIN/mediagoblin/
./lazyserver.sh --server-name=fcgi fcgi_host=127.0.0.1 fcgi_port=26543 </dev/null &>/dev/null &
# http://mediagoblin.readthedocs.org/en/latest/siteadmin/deploying.html
