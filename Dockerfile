FROM phusion/baseimage:0.9.9
MAINTAINER Andrei Cristian Petcu <andrei@ceata.org>


# base image 0.9.9
RUN echo "export GMG_DOMAIN=mygnumediagoblin" | sudo tee /etc/profile.d/gnumediagoblin.sh
RUN echo "export GMG_SENDER_EMAIL=goblin@gnumedia" | sudo tee -a /etc/profile.d/gnumediagoblin.sh

RUN sudo chmod +x /etc/profile.d/gnumediagoblin.sh
RUN sudo chmod o+r /etc/profile.d/gnumediagoblin.sh

#RUN source /etc/profile.d/gnumediagoblin.sh

RUN apt-get update
RUN sudo apt-get install git-core python python-dev python-lxml python-imaging python-virtualenv postgresql postgresql-client python-psycopg2 nginx-full wget -y

#packages for plugins
#video and audio
RUN sudo apt-get install python-gst0.10 gstreamer0.10-plugins-base gstreamer0.10-plugins-bad gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly gstreamer0.10-ffmpeg python-numpy python-scipy libsndfile1-dev libasound2-dev -y

#pdf and office stuff
RUN sudo apt-get install poppler-utils unoconv libreoffice-common -y

RUN echo "sudo -u postgres /usr/lib/postgresql/9.1/bin/postgres --config-file=/etc/postgresql/9.1/main/postgresql.conf > /dev/null 2>&1 &" | tee /etc/profile.d/start_postgres.sh
RUN sudo chmod +x /etc/profile.d/start_postgres.sh

RUN /etc/profile.d/start_postgres.sh && sleep 2s && sudo -u postgres createuser -A -D --no-createrole mediagoblin && sudo -u postgres createdb -O mediagoblin mediagoblin

RUN sudo adduser --system mediagoblin

RUN sudo groupadd mediagoblin

RUN sudo usermod -a -G mediagoblin mediagoblin
#RUN sudo usermod -a -G mediagoblin $USER

RUN sudo mkdir -p /srv/$GMG_DOMAIN && sudo chown -hR mediagoblin:mediagoblin /srv/$GMG_DOMAIN

RUN sudo -u mediagoblin /bin/bash

#RUN /bin/bash source /etc/profile.d/gnumediagoblin.sh

#WORKDIR /srv/$GMG_DOMAIN

RUN git clone git://gitorious.org/mediagoblin/mediagoblin.git
WORKDIR mediagoblin
RUN git submodule init && git submodule update

RUN (virtualenv --system-site-packages . || virtualenv .) && ./bin/python setup.py develop

RUN ./bin/easy_install flup

RUN cp mediagoblin.ini mediagoblin_local.ini

RUN sed -i "s/email_sender_address.*/email_sender_address = \"$GMG_SENDER_EMAIL\"/g" mediagoblin_local.ini
RUN sed -i "s/.*sql_engine.*/sql_engine = postgresql:\/\/\/mediagoblin/g" mediagoblin_local.ini

RUN ./bin/pip install scikits.audiolab

RUN /bin/gmg dbupdate

# ./lazyserver.sh --server-name=broadcast

WORKDIR /srv/$GMG_DOMAIN/
RUN wget https://gitlab.com/andreicristianpetcu/install-gnumediagoblin/raw/master/nginx.conf
RUN sed -i "s/mediagoblin.example.org/$GMG_DOMAIN/g" nginx.conf
RUN exit 

RUN source /etc/profile.d/gnumediagoblin.sh

# sudo ln -s /srv/$GMG_DOMAIN/nginx.conf /etc/nginx/conf.d/
RUN sudo ln -s /srv/$GMG_DOMAIN/nginx.conf /etc/nginx/sites-enabled/"$GMG_DOMAIN".nginx.conf

# sudo chmod g+r /srv/$GMG_DOMAIN/mediagoblin/user_dev/crypto/itsdangeroussecret.bin 

RUN sudo /etc/init.d/nginx restart

# <mgv> si anume chmod 755 /srv/ceata.org/medii/mediagoblin/user_dev
# <mgv> toate fisierele din user_dev sunt 644
# <mgv> dar user_dev in sine e 700

RUN sudo -u mediagoblin /bin/bash
RUN source /etc/profile.d/gnumediagoblin.sh
WORKDIR /srv/$GMG_DOMAIN/mediagoblin/
RUN echo "cd /srv/ceata.org/medii/mediagoblin/
./lazyserver.sh --server-name=fcgi fcgi_host=127.0.0.1 fcgi_port=26543 > medii.log 2>&1 "| tee /etc/my_init.d/start_gnu_media_goblin.sh
RUN chmod +x /etc/my_init.d/start_gnu_media_goblin.sh

EXPOSE 9091

ENTRYPOINT ["/sbin/my_init"]
CMD ["--enable-insecure-key"]
.
